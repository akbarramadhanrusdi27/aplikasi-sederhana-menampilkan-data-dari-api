<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ProductModel;
use Illuminate\Http\Request;

class ProdutApiController extends Controller
{
    public function index() {
        $items = ProductModel::all();
        return response()->json([
            'message'=>'Success',
            'data' => $items,
        ]);
    }
}
