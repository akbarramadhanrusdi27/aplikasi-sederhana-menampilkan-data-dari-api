import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Post {
  final int id;
  final String title;
  final String content;
  final String photo;

  Post({
    required this.id,
    required this.title,
    required this.content,
    required this.photo,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
        id: json['id'],
        title: json['title'],
        content: json['content'],
        photo: json['photo']);
  }
}

class PostsPage extends StatefulWidget {
  const PostsPage({super.key});

  @override
  State<PostsPage> createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  final String apiUrl = 'http://192.168.138.239:8000/api/posts';

  Future<List<Post>> fetchPosts() async {
    final response = await http.get(Uri.parse(apiUrl));
    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body);
      return data.map((json) => Post.fromJson(json)).toList();
    } else {
      throw Exception('failed');
    }
  }

  Future<List<String?>> getImageUrls(List<Post> posts) async {
    final List<String?> imageUrls = [];

    for (final post in posts) {
      final imageUrl = await getImageUrl(post.photo);
      imageUrls.add(imageUrl);
    }

    return imageUrls;
  }

  Future<String?> getImageUrl(String photo) async {
    final String imageUrl = 'http://192.168.138.239:8000/storage/$photo';

    try {
      final response = await Dio().get(imageUrl);
      if (response.statusCode == 200) {
        return imageUrl;
      }
    } catch (e) {
      print('Failed to load image: $e');
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Post Page'),
      ),
      body: FutureBuilder<List<Post>>(
        future: fetchPosts(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          } else {
            final posts = snapshot.data!;
            return ListView.builder(
              itemCount: posts.length,
              itemBuilder: (context, index) {
                final post = posts[index];
                final imageUrl = post.photo;
                return ListTile(
                  title: Text(post.title),
                  subtitle: Text(post.content),
                  leading: imageUrl != null
                      ? Image.network(imageUrl)
                      : Icon(Icons.image),
                );
              },
            );
          }
        },
      ),
    );
  }
}
